from django.db import models
from django.urls import reverse


class Products(models.Model):
    objects = None
    name = models.CharField(verbose_name='Наименование продукта', max_length=200)
    info = models.CharField(verbose_name='Описание продукта', max_length=200)
    price = models.FloatField(verbose_name='Цена продукта', default=0)

    def __str__(self):
        return f' {self.name} // {self.info} // {self.price}грн'

    class Meta:
        verbose_name = 'Продукт'
        verbose_name_plural = 'Продукти'

    def get_absolute_url(self):
        return reverse('shop:product_detail',
                       args=[self.id, self.slug])
