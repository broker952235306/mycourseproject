from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpRequest
from .models import Products
from django.template import loader
from cart.forms import CartProductForm


def product_list(request):
    products = Products.objects.filter(available=True)
    return render(request,
                  'shop/product/list.html',
                  {'products': products})


def product_detail(request, id):
    product = get_object_or_404(Products,
                                id=id,
                                available=True)
    cart_product_form = CartProductForm()
    return render(request, 'shop/detail.html', {'product': product,
                                                        'cart_product_form': cart_product_form})


def index(request):
    products_list = Products.objects.order_by('name')
    return render(request, 'shop/title_list.html', {
        'products_list': products_list
    })


def detail(request, name_id):
    description = get_object_or_404(Products, pk=name_id)
    return render(request, 'shop/detail.html', {
        'description': description
    })



